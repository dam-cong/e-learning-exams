package com.hoanghung.e_learning_exams.lessoncontent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hoanghung.e_learning_exams.R;
import com.hoanghung.e_learning_exams.model.APIObject;
import com.hoanghung.e_learning_exams.model.APIService;
import com.hoanghung.e_learning_exams.model.AccountManager;
import com.hoanghung.e_learning_exams.model.RetrofitClient;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class LessonContentYoutube extends AppCompatActivity {
    AccountManager accountManager;
    ListView listTest;
    ImageView imageBack;
    TextView txtTitle;
    Bundle getBundle;
    Intent getIntent;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_lesson_youtube);
        mapping();
        setListTest();
    }

    private void setListTest() {
        int courseContentDataId = getBundle.getInt("courseContentDataId");
        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<TypeOfTest>> call = service.getTypeOfTest(courseContentDataId);
        call.enqueue(new Callback<APIObject<TypeOfTest>>() {
            @Override
            public void onResponse(Call<APIObject<TypeOfTest>> call, retrofit2.Response<APIObject<TypeOfTest>> response) {
                if(response.body() != null){
                    ListTestAdapter listTestAdapter = new ListTestAdapter(LessonContentYoutube.this, R.layout.design_list_test, response.body().getData());
                    listTest.setAdapter(listTestAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<TypeOfTest>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void mapping() {
        getIntent = getIntent();
        if(getIntent.getBundleExtra("bundle") != null){
            getBundle = getIntent.getBundleExtra("bundle");
        }

        accountManager = new AccountManager(getApplicationContext());
        listTest = (ListView) findViewById(R.id.list_test);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");

        txtTitle = (TextView) findViewById(R.id.title_inside_home);
        txtTitle.setText(bundle.getString("title"));

        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        Animation animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animationFade);
                onBackPressed();
                finish();
            }
        });
    }
}
