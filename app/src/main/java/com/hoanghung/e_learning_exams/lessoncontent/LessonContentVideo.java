package com.hoanghung.e_learning_exams.lessoncontent;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.hoanghung.e_learning_exams.R;
import com.hoanghung.e_learning_exams.model.APIObject;
import com.hoanghung.e_learning_exams.model.APIService;
import com.hoanghung.e_learning_exams.model.AccountManager;
import com.hoanghung.e_learning_exams.model.RetrofitClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class LessonContentVideo extends AppCompatActivity{

    ImageView imageBack;
    TextView txtTitle;
    Animation animationFade;
    ImageView imageVideoNull;
    TextView txtVideoNull;
    String URLvideo = "Không có video nào ở đây cả";
    String URL = "";
    private int currentPosition, checkedPosition = 0;
    VideoView videoView;
    MediaPlayer mediaPlayer;
    VideoControllerView controller;
    ListView listTest;
    ProgressBar progressBar;
    FrameLayout frameVideo;
    Bundle getBundle;
    Intent getIntent;
    AccountManager accountManager;
    Boolean LANDSCAPE = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_content);
//        EventBus.getDefault().register(this);
        mapping();
        setCustomActionBar(true);
        setListTest();
    }

    private void mapping() {
        accountManager = new AccountManager(getApplicationContext());
        videoView = (VideoView) findViewById(R.id.video_view);
        txtVideoNull = (TextView) findViewById(R.id.text_video_null);
        imageVideoNull = (ImageView) findViewById(R.id.img_video_null);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        frameVideo = (FrameLayout) findViewById(R.id.frame_video);
        listTest = (ListView) findViewById(R.id.list_test);
    }

    private void setListTest() {
        getIntent = getIntent();
        if(getIntent.getBundleExtra("bundle") != null){
            getBundle = getIntent.getBundleExtra("bundle");
        }

        int courseContentDataId = getBundle.getInt("courseContentDataId");
        APIService service = RetrofitClient.getClient().create(APIService.class);
        Call<APIObject<TypeOfTest>> call = service.getTypeOfTest(courseContentDataId);
        call.enqueue(new Callback<APIObject<TypeOfTest>>() {
            @Override
            public void onResponse(Call<APIObject<TypeOfTest>> call, retrofit2.Response<APIObject<TypeOfTest>> response) {
                if(response.body() != null){
                    ListTestAdapter listTestAdapter = new ListTestAdapter(LessonContentVideo.this, R.layout.design_list_test, response.body().getData());
                    listTest.setAdapter(listTestAdapter);
                }
            }

            @Override
            public void onFailure(Call<APIObject<TypeOfTest>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setCustomActionBar(Boolean aaBar) {

        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar_inside_home);
        getSupportActionBar().setElevation(0);
        ActionBar actionBar = getSupportActionBar();
        if(aaBar == true){
            actionBar.show();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }else {
            actionBar.hide();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        txtTitle = (TextView) findViewById(R.id.title_inside_home);
        txtTitle.setText(bundle.getString("title"));

        imageBack = (ImageView) findViewById(R.id.ic_back_to_home);
        animationFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_fade);

        imageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBack.startAnimation(animationFade);
                onBackPressed();
                finish();
            }
        });
    }
}
