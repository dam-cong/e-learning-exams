package com.hoanghung.e_learning_exams.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.hoanghung.e_learning_exams.R;
import com.hoanghung.e_learning_exams.home.HomeActivity;
import com.hoanghung.e_learning_exams.homecontent.HomeContentActivity;
import com.hoanghung.e_learning_exams.login.Login;
import com.hoanghung.e_learning_exams.model.AccountManager;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    AccountManager accountManager;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {
            synchronized (this) {
                wait(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_splash);
        accountManager = new AccountManager(this);
        new CountDownTimer(0, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (accountManager.getHasLogin()) {
                    intent = new Intent(SplashActivity.this, HomeContentActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, Login.class);
                }
                startActivity(intent);
                finish();
            }
        }.start();

    }

}
